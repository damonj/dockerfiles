FROM httpd:2.4
COPY ./public-html/ /usr/local/apache2/htdocs/
COPY ./httpd.conf /usr/local/apache2/conf/httpd.conf
RUN chgrp -R 0 /usr/local/apache2 && chmod -R g=u /usr/local/apache2
EXPOSE 8080
